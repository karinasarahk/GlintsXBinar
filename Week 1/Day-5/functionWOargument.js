// declaring a function

function hello() {
    console.log("Hello World!");
    console.log("this is my first function");
}

function helloTwo() {
    console.log("this is my second function!");
}

/* printing
            things */

hello(); //call hello function
helloTwo(); // call hellotwo function
hello(); // recall hello function

console.log(Math.PI);
