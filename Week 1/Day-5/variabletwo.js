// using let
let a = 14;
console.log(a)

// or
let b;
b = 2; // assignment of b
console.log(b);

// using var
var i = 10;
console.log(i);

// or
var j;
j = 20; // assignment of j
console.log(j);

/* using const */
const x = 4 //  only way to declare constant variable
console.log(x);
