const { Square, Rectangle , Beam, Triangle, Cube, Tube, Cone } = require("./geometry");

let trySquare = new Square(17);
trySquare.calculateArea();

let tryRectangle = new Rectangle(11, 12);
tryRectangle.calculateArea();

let tryBeam = new Beam(30, 110);
tryBeam.calculateVolume();

let tryCube = new Cube(5);
tryCube.calculateVolume();

let tryTube = new Tube(10, 13);
tryTube.calculateVolume();od