const ThreeDimension = require("./threeDimension");

class Cone extends ThreeDimension {
  constructor(radius, height) {
    super("cone");
    this.radius = radius;
    this.height = height;
  }

  // overriding
  introduce() {
    super.introduce();
    console.log(`All ${this.name}s are ${this.type}. \n`);
  }

  // Overiding
  calculateVolume() {
    super.calculateVolume();
    let volume = MATH.PI * (this.radius ** 2) * this.height / 3;
    console.log(`The volume of this ${this.name} is ${volume} cm3 \n`);
  }
}

module.exports = Cone;
