const TwoDimension = require("./twoDimension");

class Rectangle extends TwoDimension {
  constructor(length, width) {
    super("rectangle");
    this.length = length;
    this.width = width;
  }

  // overriding
  introduce() {
    super.introduce();
    console.log(`All ${this.name}s are ${this.type}. \n`);
  }

  // Overiding
  calculateArea() {
    super.calculateArea();
    let area = this.length * this.width;
    console.log(`The area of this ${this.name} is ${area} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 2 * (this.length + this.width);
    console.log(`The circumference of this ${this.name} is ${circumference} cm \n`);
  }
}

module.exports = Rectangle;
