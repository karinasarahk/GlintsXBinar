const TwoDimension = require("./twoDimension");

class Triangle extends TwoDimension {
  constructor(base, height) {
    super("triangle");
    this.base = base;
    this.height = height;
  }

  // overriding
  introduce() {
    super.introduce();
    console.log(`All ${this.name}s are ${this.type}. \n`);
  }

  // Overiding
  calculateArea() {
    super.calculateArea();
    let area = this.base * this.height / 2;
    console.log(`The area of this ${this.name} is ${area} cm2 \n`);
  }

  calculateCircumference() {
    super.calculateCircumference();
    let circumference = 3 * this.base;
    console.log(`The circumference of this ${this.name} is ${circumference} cm \n`);
  }
}

module.exports = Triangle;

