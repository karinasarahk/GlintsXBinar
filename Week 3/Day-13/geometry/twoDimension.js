const Geometry = require("./geometry");

class TwoDimension extends Geometry {
    constructor(name) {
        super(name, "2D");
    }
    // using Overriding method

    introduce() {
        super.introduce();
        console.log(`Now this is a ${this.type} geometry.`);
    }
    calculateArea() {
        console.log(`Let us find the area of this ${this.name}.`);
    }

    calculateCircumference() {
        console.log(`And now, the circumference of this ${this.name}.`)
    }
}

module.exports = TwoDimension;