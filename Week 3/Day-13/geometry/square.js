const TwoDimension = require("./twoDimension");

class Square extends TwoDimension {
    constructor(length) {
        super("square");
        this.length = length;
    }
    // overriding method
    introduce() {
        super.introduce();
        console.log(`All ${this.name}s are ${this.type}. \n`);
    }
    // Overriding
    calculateArea() {
        super.calculateArea();
        let area = this.length ** 2;
        console.log(`The area of this ${this.name} is ${area} cm2. \n`);

    }
    // Overriding
    calculateCircumference() {
        super.calculateCircumference();
        let circumference = 4 * this.length;
        console.log(`The area of this ${this.name} is ${circumference} cm. \n`);
     }
}

























/* const TwoDimension = require("./twoDimension");

class Square extends TwoDimension {
    constructor(length) {
        super("Square");
        this.length = length;
    }

    // Overloading method
    introduce(intro) {
        super.introduce();
        console.log(`${intro}, this is ${this.name}`);
    }

    // Overriding
    calculateArea() {
        super.calculateArea();
        let area = this.length ** 2;

        console.log(`This area is ${area} cm2 \n`);
    }

    calculateCircumference() {
        super.calculateCircumference();
        let circumference = 4 * this.length;

        console.log(`This circumference is ${circumference}`)
    }
}

let squareOne = new Square(15);
console.log(squareOne);
squareOne.introduce("Reza");
squareOne.calculateArea();
squareOne.calculateCircumference();

*/