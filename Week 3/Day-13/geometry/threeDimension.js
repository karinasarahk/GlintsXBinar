const Geometry = require("./geometry");

class ThreeDimension extends Geometry {
    constructor(name) {
        super(name, "3D");
    }
    // Overriding method

    introduce() {
        super.introduce();
        console.log(`Now this is a ${this.type} geometry.`);
    }
    calculateVolume() {
        console.log(`Let us find the area of this ${this.name}.`);
    }
}

module.exports = ThreeDimension;