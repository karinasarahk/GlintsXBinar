const ThreeDimension = require("./threeDimension");

class Cube extends ThreeDimension {
  constructor(length) {
    super("cube");
    this.length = length;
  }

  // overriding
  introduce() {
    super.introduce();
    console.log(`All ${this.name}s are ${this.type}. \n`);
  }

  // Overiding
  calculateVolume() {
    super.calculateVolume();
    let volume = this.length ** 3;
    console.log(`The volume of this ${this.name} is ${volume} cm3 \n`);
  }
}

module.exports = Cube;






module.exports = Cube;
