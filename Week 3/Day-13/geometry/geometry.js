class Geometry {
    constructor(name, type) {
      this.name = name;
      this.type = type;
    }

    introduce() {
      console.log("Today, we are learning about Geometry.")
    }
  }
  
  module.exports = Geometry;